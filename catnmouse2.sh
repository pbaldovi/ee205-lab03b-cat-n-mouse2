#!/bin/bash
# Paulo Baldovi
# 02 Feb 2022

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

GUESS_VALUE=-1

while [ $GUESS_VALUE -ne $THE_NUMBER_IM_THINKING_OF ]
do
   echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:"
   read GUESS_VALUE
   
   if [ $GUESS_VALUE -lt 1 ]
   then
      echo "You must enter a number that's >=1"
   elif [ $GUESS_VALUE -gt $THE_MAX_VALUE ]
   then
      echo "You must enter a number that's <= $THE_MAX_VALUE"
   elif [ $GUESS_VALUE -gt $THE_NUMBER_IM_THINKING_OF ]
   then
      echo "No cat... the number I'm thinking of is smaller than $GUESS_VALUE"
   elif [ $GUESS_VALUE -lt $THE_NUMBER_IM_THINKING_OF ]
   then
      echo "No cat... the number I'm thinking of is larger than $GUESS_VALUE"
   fi
done

echo You got me.
echo "|\___/|"
echo "| o_o |"
echo " \_^_/"
